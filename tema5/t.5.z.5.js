var arr = [
    [3, 14, -6, 2, 12, -1],     
    [-9, 5, 16, -3, 2, 4], 
    [0, -12, 16, -7, -12, 8]  ]; 
   
  var result = [];
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    for (var l = 0; l < arr[i].length; l++) {
      if (arr[i][l] > 0) sum += arr[i][l];
    }
    result[i] = sum;
    sum = 0;
  }
  alert(result); // (7, 24, 25)
  